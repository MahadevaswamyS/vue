const axios = require("axios");
const base_url = 'http://localhost:8000';
import { setAuthorization } from './general';

export function login(credentials) {
  return new Promise((res, rej) =>{
    axios.post(base_url+'/api/auth/login',credentials).then((response) => {
      setAuthorization(response.data.access_token);
      res(response.data);
    }).catch((err) => {
      if (err.response.data.errors.result) {
        var error_data = {'error_name':'Wrong email or password'};
        rej(error_data);
      }else {
        rej(err.response.data.errors);
      }

    })
  })
}

export function register(credentials){
  return new Promise((resolve, reject) => {
    axios.post(base_url+'/api/auth/register',credentials).then((response) => {
      setAuthorization(response.data.access_token);
      resolve(response.data);
    }).catch((err) => {
      reject(err.response.data.errors);
    })
  })
}

export function getLocalUser(){
  const userStr = localStorage.getItem('user');
  if (!userStr) {
    return null;
  }
  return JSON.parse(userStr);
}

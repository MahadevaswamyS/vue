
import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import { routes } from './router'
import StoreData from './stores/store'
import {initialize} from './helpers/general'


Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(Vuex)

const store = new Vuex.Store(StoreData)
const router = new VueRouter({
  routes,
  mode : 'history'
})

initialize(store,router);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

import Home from './components/Home.vue';
import About from './components/About.vue';
import Contact from './components/Contact.vue';

import Login from './auth/Login';
import Register from './auth/Register'
import Dashboard from './profile/Dashboard'

export const routes = [
    {path : '/', component : Home },
    {path : '/about', component : About },
    {path : '/contact', component : Contact },
    {path : '/login', component : Login},
    {path : '/register', component : Register},
    {path : '/dashboard', component : Dashboard, meta:{ requiresAuth: true}}

]